from mtgsdk import Card
from mtgsdk import Set
from mtgsdk import Type
from mtgsdk import Supertype
from mtgsdk import Subtype
import requests
import os.path
from io import open as iopen
from os import listdir
from os import makedirs


# cards = Card.all()
#
# cards = Card.where(supertypes='legendary') \
#             .where(types='creature') \
#             .where(colors='red,white') \
#             .all()
#
# cards = Card.where(page=50).where(pageSize=50).all()
dir_path = os.path.dirname(os.path.realpath(__file__))
save_path = os.path.join(dir_path, "MTG")
if not os.path.isdir(save_path):
    makedirs(save_path)

files = listdir(dir_path)

cards = Card.where(name='Siege-Gang').all()
founds = []
for card in cards:
    new = True
    for found in founds:
        if card.name == found[0]:
            new = False
            found[1].append(card.set)
            found[2].append(card.multiverse_id)
            break
    if new:
        founds.append((card.name, [card.set], [card.multiverse_id]))

for card in founds:
    url = 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=' + str(card[2][0]) + '&type=card'
    file_name = "{}{}".format(str(card[0]), '.png')
    if file_name not in files:
        re = requests.get(url)
        complete_name = os.path.join(save_path, file_name)
        with iopen(complete_name, 'wb') as file:
            file.write(re.content)
            file.close()
