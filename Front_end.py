import sys
import os.path
from PyQt5.QtCore import Qt
from os import listdir
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget,\
    QPushButton, QHBoxLayout, QLineEdit, QLabel, QListWidget, QListWidgetItem
from PyQt5.QtGui import QPixmap
import FindCards


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.main_widget = QWidget(self)
        self.top = 100
        self.left = 100
        self.width = 500
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)
        self.card_name = ''
        self.finds = []
        self.thread = []
        # #create layout
        hbox1 = QHBoxLayout()
        hbox1.addStretch(1)
        hbox2 = QHBoxLayout()
        hbox2.addStretch(1)
        vbox1 = QVBoxLayout()
        vbox1.addStretch(1)
        self.label = QLabel(self)
        dir_path = os.path.dirname(os.path.realpath(__file__))
        database_t = os.path.join(dir_path, "MTG")
        self.database = os.path.join(database_t, "Collections.db")
        self.imag_dir = os.path.join(database_t, "cards")
        def_im = os.path.join(self.imag_dir, "None.jpg")
        self.pic = QPixmap(def_im)
        self.label.setPixmap(self.pic)
        self.label.setGeometry(0, 0, 223, 310)
        self.resize(self.pic.width()+300, self.pic.height())
        # #create run button
        self.btn1 = QPushButton('Search', self)
        self.btn1.clicked.connect(self.start)
        self.btn1.resize(self.btn1.sizeHint())

        # #create search topic box
        self.lbl2 = QLabel()
        self.lbl2.setText('Card Name')
        self.na = QLineEdit(self)
        self.na.textChanged[str].connect(self.on_changed)

        # #create list box for search results
        self.list = QListWidget()
        self.list.itemClicked.connect(self.display)
        # #feed widgets into layout
        hbox2.addWidget(self.btn1)
        hbox2.addWidget(self.na)
        vbox1.addLayout(hbox2)
        vbox1.addWidget(self.list)
        hbox1.addLayout(vbox1)

        self.main_widget.setLayout(hbox1)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.initui()

    def initui(self):
        self.setWindowTitle('Card Gatherer')
        self.show()

    # #Function for 'run' button
    def start(self):
        self.list.clear()
        self.finds = FindCards.main(self.card_name)
        for treasure in self.finds:
            item = QListWidgetItem(treasure[1] + '; ' + treasure[2])
            item.setData(Qt.UserRole, treasure)
            self.list.addItem(item)

    # #Function for reading in topic when edited
    def on_changed(self, text):
        self.card_name = text

    def display(self, item):
        temp = item.data(Qt.UserRole)
        temp_inf = FindCards.main2(temp[0])
        pic_dir = os.path.join(self.imag_dir, str(temp_inf[0]))
        if temp_inf[1] != 'None':
            pics = listdir(pic_dir)
            for pic in pics:
                temp = pic.split('.')
                if temp[0] == temp_inf[1]:
                    pic1 = os.path.join(str(temp_inf[0]), pic)
                    self.set_image(pic1)
                    break
        else:
            self.set_image("None.jpg")

    def set_image(self, pic):
        def_im = os.path.join(self.imag_dir, pic)
        self.pic = QPixmap(def_im)
        self.label.setPixmap(self.pic)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
