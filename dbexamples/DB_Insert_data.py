import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_project(conn, project):
    sql = ''' INSERT INTO projects(name,begin_date,end_date)
              VALUES(?,?,?)'''
    cur = conn.cursor()
    cur.execute(sql, project)
    return cur.lastrowid


def create_task(conn, task):
    sql = ''' INSERT INTO tasks(name,priority,status_id,project_id,begin_date,end_date)
              VALUES(?,?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, task)
    return cur.lastrowid


def main():
    database = "F:\\Crawler\\db\\test.db"

    # create a database connection
    conn = create_connection(database)
    with conn:
        project = ('Cool App', '2015-01-01', '2015-01-30')
        project_id = create_project(conn, project)

        task_1 = ('Analyse requirements', 1, 1, project_id, '2015-01-01', '2015-01-02')
        task_2 = ('Confir top user requirements', 1, 1, project_id, '2015-01-03', '2015-01-05')

        create_task(conn, task_1)
        create_task(conn, task_2)


if __name__ == '__main__':
    main()
