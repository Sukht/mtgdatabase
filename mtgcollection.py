from mtgsdk import Card
from mtgsdk import Set
from mtgsdk import Type
from mtgsdk import Supertype
from mtgsdk import Subtype
import requests
import os.path
from io import open as iopen
from os import listdir
from os import makedirs
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def gen_tables(conn):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database = os.path.join(dir_path, "MTG\Collections.db")

    sql_create_collection_table = """ CREATE TABLE IF NOT EXISTS collections (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL
                                    ); """

    sql_create_card_table = """CREATE TABLE IF NOT EXISTS cards (
                                    id integer PRIMARY KEY,
                                    name text NOT NULL,
                                    quantity integer NOT NULL,
                                    collection_id integer NOT NULL,
                                    set text NOT NULL,
                                    multiverse_id text NOT NULL,
                                    FOREIGN KEY (collection_id) REFERENCES collections (id)
                                );"""

    # create a database connection
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_collection_table)
        # create tasks table
        create_table(conn, sql_create_card_table)
    else:
        print("Error! cannot create the database connection.")


def create_collection(conn, collection):
    sql = ''' INSERT INTO collections(name)
              VALUES(?)'''
    cur = conn.cursor()
    cur.execute(sql, collection)
    return cur.lastrowid


def create_card(conn, card):
    sql = ''' INSERT INTO cards(name,quantity,collection_id,set,multiverse_id)
              VALUES(?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, card)
    return cur.lastrowid


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database = os.path.join(dir_path, "MTG\Collections.db")

    # create a database connection
    conn = create_connection(database)
    with conn:
        gen_tables(conn)

        collection = ('Main',)
        collection_id = create_collection(conn, collection)

        card_1 = ('Timmy', 1, collection_id, 'Innistrad', '86721')
        card_2 = ('Steve', 2, collection_id, 'Core19', '687CD')

        create_card(conn, card_1)
        create_card(conn, card_2)


if __name__ == '__main__':
    main()