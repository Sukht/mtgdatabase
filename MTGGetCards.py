from mtgsdk import Card
from mtgsdk import Set
import os.path
import sqlite3
from sqlite3 import Error
import requests
import os.path
from io import open as iopen
from os import listdir
from os import makedirs


session = requests.session()
session.proxies = {}
# #For use with Tor
# session.proxies['http'] = 'socks5h://localhost:9050'
# session.proxies['https'] = 'socks5h://localhost:9050'


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)
        

def gen_tables(conn):
    sql_create_set_table = """ CREATE TABLE IF NOT EXISTS sets (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL,
                                        code text NOT NULL
                                    ); """

    sql_create_card_table = """CREATE TABLE IF NOT EXISTS cards (
                                    id integer PRIMARY KEY,
                                    name text NOT NULL,
                                    set_id integer NOT NULL,
                                    mana_cost text,
                                    colors text,
                                    rarity text,
                                    card_txt text,
                                    size text,
                                    rulings text,
                                    legalities text,
                                    multiverse_id text NOT NULL,
                                    FOREIGN KEY (set_id) REFERENCES sets (id)
                                );"""

    sql_create_collections_table = """CREATE TABLE IF NOT EXISTS collections (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL
                                    );"""

    sql_create_decks_table = """CREATE TABLE IF NOT EXISTS decks (
                                            id integer PRIMARY KEY,
                                            name text NOT NULL,
                                            format text NOT NULL
                                        );"""

    sql_create_collection_contents_table = """CREATE TABLE IF NOT EXISTS collection_contents (
                                                id integer PRIMARY KEY,
                                                quantity integer NOT NULL,
                                                card_id integer NOT NULL,
                                                set_id integer NOT NULL,
                                                collection_id integer NOT NULL,
                                                FOREIGN KEY (card_id) REFERENCES cards (id),
                                                FOREIGN KEY (collection_id) REFERENCES collections (id),
                                                FOREIGN KEY (set_id) REFERENCES sets (id)
                                            );"""

    sql_create_deck_contents_table = """CREATE TABLE IF NOT EXISTS deck_contents (
                                                    id integer PRIMARY KEY,
                                                    quantity integer NOT NULL,
                                                    card_id integer NOT NULL,
                                                    deck_id integer NOT NULL,
                                                    FOREIGN KEY (card_id) REFERENCES cards (id),
                                                    FOREIGN KEY (deck_id) REFERENCES decks (id)
                                                );"""

    # create a database connection
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_set_table)
        # create tasks table
        create_table(conn, sql_create_card_table)
        create_table(conn, sql_create_collections_table)
        create_table(conn, sql_create_collection_contents_table)
        create_table(conn, sql_create_decks_table)
        create_table(conn, sql_create_deck_contents_table)
    else:
        print("Error! cannot create the database connection.")


def create_set(conn, set):
    sql = ''' INSERT INTO sets(name, code)
              VALUES(?,?)'''
    cur = conn.cursor()
    cur.execute(sql, set)
    return cur.lastrowid


def create_card(conn, card):
    sql = ''' INSERT INTO cards(name, set_id, mana_cost, colors, rarity, card_txt, size, rulings, legalities,
     multiverse_id)
              VALUES(?,?,?,?,?,?,?,?,?,?)'''
    cur = conn.cursor()
    cur.execute(sql, card)
    return cur.lastrowid


def build_db():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database = os.path.join(dir_path, "MTG\Collections.db")
    conn = create_connection(database)

    with conn:
        gen_tables(conn)

        sets = Set.all()
        for set in sets:

            setter = (set.name, set.code)
            setter_id = create_set(conn, setter)
            cards = Card.where(set=set.code).all()
            for card in cards:
                if 'Creature' in card.type:
                    if not card.power:
                        print(card.type)
                    else:
                        size = card.power + '/' + card.toughness

                elif 'Planeswalker' in card.type:
                    size = card.loyalty

                else:
                    size = ''

                colours = ''
                if card.colors:
                    for color in card.colors:
                        if not colours:
                            colours = color
                        else:
                            colours = colours + ', ' + color

                rulings = ''
                if card.rulings:
                    for ruling in card.rulings:
                        if not rulings:
                            rulings = ruling['text']
                        else:
                            rulings = rulings + '\n' + ruling['text']

                legalities = ''
                if card.legalities:
                    for legalitie in card.legalities:
                        if not legalities:
                            legalities = legalitie['format'] + ', ' + legalitie['legality']
                        else:
                            legalities = legalities + '\n' + legalitie['format'] + ', ' + legalitie['legality']

                if not card.multiverse_id:
                    carder = (card.name, setter_id, card.mana_cost, colours, card.rarity, card.text, size,
                              rulings, legalities, 'None')
                    create_card(conn, carder)

                else:
                    carder = (card.name, setter_id, card.mana_cost, colours, card.rarity, card.text, size,
                              rulings, legalities, card.multiverse_id)
                    create_card(conn, carder)


def get_images():
    global session
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database = os.path.join(dir_path, "MTG\Collections.db")
    conn = create_connection(database)

    cur = conn.cursor()
    cur.execute("SELECT * FROM cards")
    rows = cur.fetchall()
    for row in rows:
        if str(row[10]) != 'None':
            save_path = os.path.join(dir_path, "MTG\cards\{}".format(str(row[2])))
            if not os.path.isdir(save_path):
                makedirs(save_path)
            files = listdir(save_path)
            url = 'http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=' + str(row[10]) + '&type=card'
            file_name = "{}{}".format(row[10], '.png')
            if file_name not in files:
                try:
                    re = session.get(url)
                    complete_name = os.path.join(save_path, file_name)
                    with iopen(complete_name, 'wb') as file:
                        file.write(re.content)
                        file.close()
                except Exception as e:
                    print(e)


build_db()
get_images()
