import os.path
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def find_all_copies(conn, name):
    cur = conn.cursor()
    cur.execute("SELECT * FROM cards WHERE name LIKE ?", (name,))
    rows = cur.fetchall()
    find = []
    for row in rows:
        cur.execute("SELECT * FROM sets WHERE id=?", (row[2],))
        setty = cur.fetchall()
        find.append((row[0], row[1], setty[0][1]))
    return find


def find_pic(conn, ids):
    cur = conn.cursor()
    cur.execute("SELECT * FROM cards WHERE id=?", (ids,))
    rows = cur.fetchall()
    find = (rows[0][2], rows[0][10])
    return find


def main(name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database_t = os.path.join(dir_path, "MTG")
    database = os.path.join(database_t, "Collections.db")
    conn = create_connection(database)

    with conn:
        return find_all_copies(conn, '%'+name+'%')


def main2(ids):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database_t = os.path.join(dir_path, "MTG")
    database = os.path.join(database_t, "Collections.db")
    conn = create_connection(database)

    with conn:
        return find_pic(conn, ids)
