import os.path
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_collection(conn, collection):
    sql = ''' INSERT INTO collections(name)
              VALUES(?)'''
    cur = conn.cursor()
    cur.execute(sql, collection)
    return cur.lastrowid


def create_collection_content(conn, collection_contents):
    sql = ''' INSERT INTO collection_contents(quantity, card_id, set_id, collection_id)
              VALUES(?,?,?,?)'''
    cur = conn.cursor()
    cur.execute(sql, collection_contents)
    return cur.lastrowid


def add_card(conn, collection, name, set, quantity):
    cur = conn.cursor()
    cur.execute("SELECT * FROM collections WHERE name=?", (collection,))
    col_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM sets WHERE name=?", (set,))
    set_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM cards WHERE name=? AND set_id=?", (name, set_id))
    card_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM collection_contents WHERE card_id=? AND collection_id=?", (card_id, col_id))
    temp = cur.fetchall()
    if not temp:
        cont = (quantity, card_id, set_id, col_id)
        create_collection_content(conn, cont)
    else:
        cur.execute("UPDATE collection_contents SET quantity = quantity + ? WHERE card_id=? AND collection_id=?",
                    (quantity, card_id, col_id))


def remove_card(conn, collection, name, set, quantity):
    cur = conn.cursor()
    cur.execute("SELECT * FROM collections WHERE name=?", (collection,))
    col_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM sets WHERE name=?", (set,))
    set_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM cards WHERE name=? AND set_id=?", (name, set_id))
    card_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM collection_contents WHERE card_id=? AND collection_id=?", (card_id, col_id))
    temp = cur.fetchall()
    if not temp:
        print("Not in collection")
    else:
        if int(temp[0][1]) > quantity:
            cur.execute("UPDATE collection_contents SET quantity = quantity - ? WHERE id=?", (quantity, temp[0][0]))
        else:
            cur.execute("DELETE FROM collection_contents WHERE id=?", (temp[0][0],))


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database = os.path.join(dir_path, "MTG\Collections.db")
    conn = create_connection(database)

    with conn:
        col = ('Main Collection',)
        cur = conn.cursor()
        cur.execute("SELECT * FROM collections WHERE name=?", col)
        col_id = cur.fetchall()
        if not col_id:
            create_collection(conn, col)
        else:
            print(col[0] + ' already exists')

        # add_card(conn, 'Main Collection', 'Siege-Gang Commander', 'Dominaria', 4)
        remove_card(conn, 'Main Collection', 'Siege-Gang Commander', 'Dominaria', 7)


main()
