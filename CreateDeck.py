import os.path
import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_deck(conn, deck):
    sql = ''' INSERT INTO decks(name, format)
              VALUES(?,?)'''
    cur = conn.cursor()
    cur.execute(sql, deck)
    return cur.lastrowid


def create_deck_content(conn, deck_contents):
    sql = ''' INSERT INTO deck_contents(quantity, card_id, deck_id)
              VALUES(?,?,?,?)'''
    cur = conn.cursor()
    cur.execute(sql, deck_contents)
    return cur.lastrowid


def add_card(conn, deck, name, set, quantity):
    cur = conn.cursor()
    cur.execute("SELECT * FROM decks WHERE name=?", (deck,))
    col_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM sets WHERE name=?", (set,))
    set_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM cards WHERE name=? AND set_id=?", (name, set_id))
    card_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM deck_contents WHERE card_id=? AND collection_id=?", (card_id, col_id))
    temp = cur.fetchall()
    if not temp:
        cont = (quantity, card_id, col_id)
        create_deck_content(conn, cont)
    else:
        cur.execute("UPDATE deck_contents SET quantity = quantity + ? WHERE card_id=? AND collection_id=?",
                    (quantity, card_id, col_id))


def remove_card(conn, deck, name, set, quantity):
    cur = conn.cursor()
    cur.execute("SELECT * FROM decks WHERE name=?", (deck,))
    col_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM sets WHERE name=?", (set,))
    set_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM cards WHERE name=? AND set_id=?", (name, set_id))
    card_id = cur.fetchall()[0][0]
    cur.execute("SELECT * FROM deck_contents WHERE card_id=? AND deck_id=?", (card_id, col_id))
    temp = cur.fetchall()
    if not temp:
        print("Not in deck")
    else:
        if int(temp[0][1]) > quantity:
            cur.execute("UPDATE deck_contents SET quantity = quantity - ? WHERE id=?", (quantity, temp[0][0]))
        else:
            cur.execute("DELETE FROM deck_contents WHERE id=?", (temp[0][0],))


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    database = os.path.join(dir_path, "MTG\Collections.db")
    conn = create_connection(database)

    with conn:
        col = ('My First Deck', 'Modern')
        cur = conn.cursor()
        cur.execute("SELECT * FROM decks WHERE name=? AND format=?", col)
        col_id = cur.fetchall()
        if not col_id:
            create_deck(conn, col)
        else:
            print(col[0] + ' already exists')

        add_card(conn, 'My First Deck', 'Siege-Gang Commander', 'Dominaria', 4)
        # remove_card(conn, 'My First Deck', 'Siege-Gang Commander', 'Dominaria', 7)


main()
